//
//  ViewController.swift
//  SMART
//
//  Created by Debanik Purkayastha on 9/23/18.
//  Copyright © 2018 DT4. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        super.view.backgroundColor = .white
        
        let hello = UILabel()
        hello.translatesAutoresizingMaskIntoConstraints = false
        hello.text = "Welcome to S.M.A.R.T."
        hello.textColor = UIColor(red:0.00, green:0.37, blue:0.72, alpha:1.0)
        hello.textAlignment = .center
        hello.font = UIFont.boldSystemFont(ofSize: 25)
        hello.textAlignment = .center
        hello.adjustsFontSizeToFitWidth = true
        view.addSubview(hello)
        hello.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        hello.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -80).isActive = true
        hello.heightAnchor.constraint(equalToConstant: 20).isActive = true
        hello.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
        let loginButton = UIButton()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
//        proceedButton.addTarget(self, action: #selector(startTracking), for: .touchUpInside)
        loginButton.setTitle("Login", for: .normal)
        loginButton.backgroundColor = UIColor(red:0.00, green:0.37, blue:0.72, alpha:1.0)
        loginButton.layer.cornerRadius = 20
        loginButton.clipsToBounds = true
        self.view.addSubview(loginButton)
        loginButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        loginButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: view.frame.height / 10).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: view.frame.width - 20).isActive = true
        
        let registerButton = UIButton()
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        //        proceedButton.addTarget(self, action: #selector(startTracking), for: .touchUpInside)
        registerButton.setTitle("Register", for: .normal)
        registerButton.backgroundColor = UIColor(red:0.00, green:0.37, blue:0.72, alpha:1.0)
        registerButton.layer.cornerRadius = 20
        registerButton.clipsToBounds = true
        self.view.addSubview(registerButton)
        registerButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        registerButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 80).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: view.frame.height / 10).isActive = true
        registerButton.widthAnchor.constraint(equalToConstant: view.frame.width - 20).isActive = true
    }


}

